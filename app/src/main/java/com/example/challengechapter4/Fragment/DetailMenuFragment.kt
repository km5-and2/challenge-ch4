package com.example.challengechapter4.Fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.navigation.fragment.findNavController
import androidx.room.InvalidationTracker
import com.example.challengechapter4.ListItemMenu
import com.example.challengechapter4.R
import com.example.challengechapter4.ViewModel.DetailMenuViewModel
import com.example.challengechapter4.ViewModel.ViewModelFactory
import com.example.challengechapter4.databinding.FragmentDetailMenuBinding
import java.lang.NullPointerException

class DetailMenuFragment : Fragment() {

    private var _binding: FragmentDetailMenuBinding? = null
    private val binding get() = _binding!!

    private val location: String = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"

    private lateinit var viewModel: DetailMenuViewModel
    private var item: ListItemMenu? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailMenuBinding.inflate(inflater, container, false)
        setUpCartViewModel()

        viewModel.totalPrice.observe(viewLifecycleOwner) {
            binding.buttonTambahKeranjang.text = "Tambah ke Keranjang - Rp.$it"
        }
        setData()
        addTocCart()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            buttonBack()
            wViewModel()

            binding.location.setOnClickListener{
                try {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(location))
                    startActivity(intent)
                } catch (e: ActivityNotFoundException){
                    Toast.makeText(
                        requireContext(),
                        "Google Maps tidak terinstall",
                        Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }catch (e: NullPointerException){
            Toast.makeText(
                requireContext(),
                "Error: $e", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun buttonBack() {
        binding.buttonBack.setOnClickListener{
            requireActivity().onBackPressed()
        }
    }

    private fun addTocCart() {
        binding.buttonTambahKeranjang.setOnClickListener{
            viewModel.addToCart()

            findNavController().navigate(R.id.action_detailMenuFragment_to_cartFragment)
        }
    }

    private fun setUpCartViewModel() {
        val viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory)[DetailMenuViewModel::class.java]
    }

    private fun setData() {

        item = arguments?.getParcelable("item")

        item?.let {
            binding.imageDetailMenu.setImageResource(it.image)
            binding.namaDetailMenu.text = item?.namaMakanan
            binding.priceDetailMenu.text= item?.harga.toString()

            viewModel.initSelectedItem(it)
        }
    }

    private fun wViewModel() {
        val observer = Observer<Int> {
            binding.btnOrder.textTotalOrder.text = it.toString()
        }
        viewModel.counter.observe(viewLifecycleOwner, observer)

        binding.btnOrder.buttonPlus.setOnClickListener {
            viewModel.increment()
        }
        binding.btnOrder.buttonMinus.setOnClickListener {
            viewModel.decrement()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}