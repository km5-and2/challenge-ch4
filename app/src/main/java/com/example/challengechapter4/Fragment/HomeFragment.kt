package com.example.challengechapter4.Fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter4.AdapterMenu
import com.example.challengechapter4.ListItemMenu
import com.example.challengechapter4.R
import com.example.challengechapter4.SharedPreferences
import com.example.challengechapter4.ViewModel.HomeViewModel
import com.example.challengechapter4.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var userPreference: SharedPreferences

    private lateinit var menuAdapter: AdapterMenu
    private var viewList: Boolean = true
    private var typeLayout = true
    private val dataMakanan = ArrayList<ListItemMenu>()

    private val icon = arrayListOf(
        R.drawable.baseline_format_list_bulleted_24,
        R.drawable.baseline_border_all_24
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater,container, false)

        userPreference = SharedPreferences(requireContext())

        homeViewModel = ViewModelProvider(requireActivity())[HomeViewModel::class.java]
        homeViewModel.menuView.value = userPreference.getPreferenceLayout()

        menuAdapter = AdapterMenu(dataMakanan, homeViewModel.menuView.value?:true)
        binding.recyclerMakanan.adapter = menuAdapter

        binding.recyclerMakanan.setHasFixedSize(true)
        if (dataMakanan.isEmpty()){
            dataMakanan.addAll(getMakanan())
        }
        showRecycleMakanan()

        homeViewModel.menuView.observe(viewLifecycleOwner){
            setupChangeLayout()
        }

        homeViewModel.menuItem.observe(viewLifecycleOwner){ menuItem ->
            updateRecyclerview(menuItem)
        }

        itemClicked()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toggleButton = binding.buttonList
        toggleButton.setOnClickListener{
            viewList = !viewList
            setupChangeLayout()
            toggleImageView(toggleButton)
        }
        setupChangeLayout()
    }

    private fun getMakanan(): ArrayList<ListItemMenu> {
        val dataImg = resources.obtainTypedArray(R.array.images)
        val dataName = resources.getStringArray(R.array.name)
        val dataPrice = resources.getIntArray(R.array.harga)

        val listMakanan = ArrayList<ListItemMenu>()
        for (i in dataName.indices){
            val makanan = ListItemMenu(dataImg.getResourceId(i, -1), dataName[i], dataPrice[i])
            listMakanan.add(makanan)
        }
        return listMakanan
    }

    private fun showRecycleMakanan(){
        binding.recyclerMakanan.layoutManager = GridLayoutManager(requireActivity(), 2)
        val menuAdapater = AdapterMenu(dataMakanan)
        binding.recyclerMakanan.adapter = menuAdapater
    }

    private fun toggleImageView(imageView: ImageView){
        imageView.setImageResource(icon[if(viewList) 0 else 1])
    }

    private fun ShowGridLayout(){
        binding.recyclerMakanan.layoutManager = GridLayoutManager(requireActivity(), 2)
        val adapterMenu = AdapterMenu(dataMakanan, gridLayout = true)
        binding.recyclerMakanan.adapter = adapterMenu
    }

    private fun ShowLinearLayout(){
        binding.recyclerMakanan.layoutManager = LinearLayoutManager(requireActivity())
        val adapterMenu = AdapterMenu(dataMakanan, gridLayout = false)
        binding.recyclerMakanan.adapter = adapterMenu
    }

    private fun itemClicked(){
        menuAdapter = AdapterMenu(dataMakanan, homeViewModel.menuView.value?: true) { item ->

            val bundle = bundleOf("item" to item)
            findNavController().navigate(R.id.action_homeFragment_to_detailMenuFragment, args = bundle)
        }

        binding.recyclerMakanan.adapter = menuAdapter
    }

    private fun setupRV(viewList: Boolean) {
        dataMakanan.clear()

        typeLayout = if (viewList) {
            ShowGridLayout()
            true
        } else {
            ShowLinearLayout()
            false
        }

        val adapter = AdapterMenu(dataMakanan, gridLayout = typeLayout, onItemClick = {
            itemClicked()
        })

        dataMakanan.addAll(getMakanan())
        binding.recyclerMakanan.adapter = adapter
    }

    private fun updateRecyclerview(menuItem: ArrayList<ListItemMenu>){
        menuAdapter.reloadData(menuItem)
        menuAdapter.gridLayout = homeViewModel.menuView.value?: true
        binding.recyclerMakanan.adapter?.notifyDataSetChanged()
    }

    private fun setupChangeLayout(){
        val buttonImage = binding.buttonList
        val currentLayout: Boolean = homeViewModel.menuView.value?: userPreference.getPreferenceLayout()

        setupRV(currentLayout)
        buttonImage.setImageResource(if (currentLayout) R.drawable.baseline_format_list_bulleted_24 else R.drawable.baseline_border_all_24)

        buttonImage.setOnClickListener{
            val newValueList = !currentLayout
            homeViewModel.menuView.value = newValueList
            userPreference.savePreferenceLayout(newValueList)
        }

    }
}