package com.example.challengechapter4.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.challengechapter4.CartAdapter
import com.example.challengechapter4.R
import com.example.challengechapter4.ViewModel.CartViewModel
import com.example.challengechapter4.ViewModel.ViewModelFactory
import com.example.challengechapter4.databinding.FragmentCartBinding

class CartFragment : Fragment() {

    private lateinit var binding: FragmentCartBinding
    private lateinit var cartViewModel: CartViewModel
    private lateinit var cartAdapter: CartAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentCartBinding.inflate(inflater,container, false)
        setUpCartViewModel()

        cartAdapter = CartAdapter(cartViewModel, false)
        binding.rvKeranjang.setHasFixedSize(true)
        binding.rvKeranjang.layoutManager = LinearLayoutManager(requireContext())
        binding.rvKeranjang.adapter = cartAdapter

        cartViewModel.allCartItems.observe(viewLifecycleOwner) {
            cartAdapter.setData(it)

            var totalPrice = 0
            it.forEach { item -> totalPrice += item.totalPrice
            }
            val textPrice = "Rp. $totalPrice"
            binding.textTotalHarga.text = textPrice
        }

        addToConfirm()
        return binding.root
    }

    private fun setUpCartViewModel() {
        val viewModelFactory = ViewModelFactory(requireActivity().application)
        cartViewModel = ViewModelProvider(this, viewModelFactory)[CartViewModel::class.java]
    }

    private fun addToConfirm() {
        binding.BtnKeranjang.setOnClickListener {
            findNavController().navigate(R.id.action_cartFragment_to_confirmOrderFragment)
        }
    }
}