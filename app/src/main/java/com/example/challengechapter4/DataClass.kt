package com.example.challengechapter4

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ListItemMenu(
    val image : Int,
    val namaMakanan: String,
    val harga : Int

):Parcelable