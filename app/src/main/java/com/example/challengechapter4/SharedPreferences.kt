package com.example.challengechapter4

import android.content.Context
import android.content.SharedPreferences

class SharedPreferences (context: Context) {

    companion object{
        private const val USER = "User SharedPreferences"
        private const val KEY = "Key SharedPreferences"
    }

    private val preferences: SharedPreferences = context.getSharedPreferences(USER, 0)

    fun getPreferenceLayout () : Boolean {
        return preferences.getBoolean(KEY, true)
    }

    fun savePreferenceLayout (isListView: Boolean){
        preferences.edit().putBoolean(KEY, isListView).apply()
    }
}