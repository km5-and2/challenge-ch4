package com.example.challengechapter4.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Cart::class], version = 1)
abstract class CartDatabase : RoomDatabase(){
    abstract val cartDao: CartDao

    companion object{

        private var INSTANCE: CartDatabase? = null

        fun getInstances(context: Context): CartDatabase {
            if(INSTANCE == null){
                synchronized(CartDatabase::class.java){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        CartDatabase::class.java, "cart_database"
                    ).build()
                }
            }
            return INSTANCE as CartDatabase
        }
    }
}