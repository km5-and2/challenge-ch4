package com.example.challengechapter4.Database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface CartDao {
    @Insert
    fun insert(cart: Cart)

    @Query("SELECT * FROM menu_cart ORDER BY id DESC")
    fun getAllItem(): LiveData<List<Cart>>

    @Update
    fun update(cart: Cart)

    @Delete
    fun delete(cart: Cart)

    @Query("DELETE FROM menu_cart WHERE id = :itemId")
    fun deleteItemById(itemId: Long)
}