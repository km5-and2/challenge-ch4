package com.example.challengechapter4.Database

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.challengechapter4.Database.Cart
import com.example.challengechapter4.Database.CartDao
import com.example.challengechapter4.Database.CartDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class CartRepo(application: Application){

    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()
    private val _cartDao: CartDao

    init {
        val database = CartDatabase.getInstances(application)
        _cartDao = database.cartDao
    }

    fun insert(cart: Cart){
        executorService.execute { _cartDao.insert(cart) }
    }

    fun getAllCartItems(): LiveData<List<Cart>> = _cartDao.getAllItem()

    fun deleteItemCart(cartId : Long){
        executorService.execute{ _cartDao.deleteItemById(cartId) }
    }

    fun quantityUpdates(cart: Cart) {
        executorService.execute { _cartDao.update(cart) }
    }
}