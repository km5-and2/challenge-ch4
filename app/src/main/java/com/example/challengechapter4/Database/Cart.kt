package com.example.challengechapter4.Database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "menu_cart")
data class Cart (
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    @ColumnInfo(name = "food_Name") var foodName: String,
    @ColumnInfo(name = "image_Id") var imageId: Int,
    @ColumnInfo(name = "food_Price") var price: Int,
    @ColumnInfo(name = "food_Quantity") var quantity: Int,
    @ColumnInfo(name = "total_Price") var totalPrice: Int,
    @ColumnInfo(name = "food_Note") var foodNote: String? = null

) : Parcelable