package com.example.challengechapter4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.challengechapter4.Database.Cart
import com.example.challengechapter4.ViewModel.CartViewModel
import com.example.challengechapter4.databinding.OrderCartBinding

class CartAdapter (
    private val cartViewModel: CartViewModel,
    private val confirmOrder: Boolean): RecyclerView.Adapter<CartAdapter.CartViewHolder>() {

    private var cartItems: List<Cart> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = OrderCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun getItemCount(): Int = cartItems.size

    override fun onBindViewHolder(holder: CartAdapter.CartViewHolder, position: Int) {
        val currentItem = cartItems[position]
        holder.bind(currentItem, viewModel = cartViewModel, confirmOrder)
    }

    class CartViewHolder(private val binding: OrderCartBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(cartItem: Cart, viewModel: CartViewModel, confirmOrder: Boolean) {

                if (confirmOrder){
                    binding.ivDelete.visibility = View.GONE
                    binding.btnMinus.visibility = View.INVISIBLE
                    binding.btnPlus.visibility = View.INVISIBLE
                    binding.ivFood.setImageResource(cartItem.imageId)
                    binding.price.text = cartItem.price.toString()
                    binding.tvDescription.text = cartItem.foodName
                    binding.tvNumber.text = cartItem.quantity.toString()
                }
                else {
                    binding.tvDescription.text = cartItem.foodName
                    binding.ivFood.setImageResource(cartItem.imageId)
                    binding.price.text = cartItem.price.toString()
                    binding.tvNumber.text = cartItem.quantity.toString()
                }

                binding.ivDelete.setOnClickListener{
                    viewModel.deletedItemCart(cartItem.id)
                }

                binding.btnPlus.setOnClickListener {
                    viewModel.increment(cartItem)
                }

                binding.btnMinus.setOnClickListener {
                    viewModel.decrement(cartItem)
                }
            }
        }
    fun setData(cartItems: List<Cart>) {
        this.cartItems = cartItems
        notifyDataSetChanged()
    }
}