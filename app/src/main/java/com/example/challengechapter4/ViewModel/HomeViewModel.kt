package com.example.challengechapter4.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechapter4.ListItemMenu

class HomeViewModel: ViewModel (){

    val menuView = MutableLiveData<Boolean>().apply { value = true }
    val menuItem = MutableLiveData<ArrayList<ListItemMenu>>()

}