package com.example.challengechapter4.ViewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.challengechapter4.Database.Cart
import com.example.challengechapter4.Database.CartRepo

class CartViewModel(application: Application) : ViewModel() {

    private val repository: CartRepo = CartRepo(application)

    val allCartItems: LiveData<List<Cart>> = repository.getAllCartItems()

    fun deletedItemCart(cartId: Long) {
        repository.deleteItemCart(cartId)
    }

    private fun updateItem(cart: Cart) {
        repository.quantityUpdates(cart)
    }

    fun increment(cart: Cart) {
        val newTotal = cart.quantity + 1
        cart.quantity = newTotal
        cart.totalPrice = cart.price * newTotal

        updateItem(cart)
    }

    fun decrement(cart: Cart) {
        val newTotal = cart.quantity - 1
        cart.quantity = newTotal
        cart.totalPrice = cart.price * newTotal

        updateItem(cart)
    }

}